from PyQt5.QtCore import QAbstractListModel, QModelIndex, Qt, QUrl, QVariant
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQuick import QQuickView
from PyQt5.QtQml import QQmlEngine, QQmlComponent, QQmlApplicationEngine
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QObject, pyqtSlot, pyqtProperty


class Animal(object):

    def __init__(self, type, size):
        self._type = type
        self._size = size

    def type(self):
        return self._type

    def size(self):
        return self._size


class AnimalModel(QAbstractListModel):

    TypeRole = Qt.UserRole + 1
    SizeRole = Qt.UserRole + 2

    _roles = {TypeRole: b"type", SizeRole: b"size"}

    def __init__(self, parent=None):
        super(AnimalModel, self).__init__(parent)

        self._animals = []

    def addAnimal(self, animal):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self._animals.append(animal)
        self.endInsertRows()

    def rowCount(self, parent=QModelIndex()):
        return len(self._animals)

    def data(self, index, role=Qt.DisplayRole):
        try:
            animal = self._animals[index.row()]
        except IndexError:
            return QVariant()

        if role == self.TypeRole:
            return animal.type()

        if role == self.SizeRole:
            return animal.size()

        return QVariant()

    def roleNames(self):
        return self._roles

class MainModel(QObject):

    def __init__(self, parent = None):
        QObject.__init__(self, parent)
        self._modelAnimal = AnimalModel()
        self._modelAnimal.addAnimal(Animal("Wolf", "Medium"))
        self._modelAnimal.addAnimal(Animal("Polar bear", "Large"))
        self._modelAnimal.addAnimal(Animal("Quoll", "Small"))

    @pyqtProperty(AnimalModel)
    def modelAnimal(self):
        return self._modelAnimal



if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    model = MainModel()
    engine = QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("_model", model)
    engine.load('main.qml')

    engine.quit.connect(app.quit)
    sys.exit(app.exec_())

