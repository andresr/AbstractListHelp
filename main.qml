import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    property var modelListView: ""


    Component.onCompleted: {
           modelListView = _model.modelAnimal
       }

    ListView {
        id: listView
        anchors.fill: parent
        model: modelListView
        delegate: Item {
            implicitHeight: text.height
            width: listView.width
            Text {
                id: text
                text: model.type + " " + model.size
            }

       }
    }

}
